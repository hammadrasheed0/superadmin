<?php

Route::group(['middleware' => 'web', 'prefix' => 'bo', 'namespace' => 'Modules\Bo\Http\Controllers'], function()
{
    Route::get('/', 'BoController@index');
});
